resource "aws_instance" "my1stinstance" {
  ami           = "ami-079db87dc4c10ac91"
  instance_type = "t2.micro"

  tags = {
    Name           = "cicd-gitlab-instance"
    Created_by     = "Anil"
    GitLab_Group   = "Terraform-group"
    GitLab_Project = "anil-tf-iac"
  }
}
